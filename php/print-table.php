<?php 
$sno = 1;
if (isset($_POST['print'])) {
	$rows = $_POST['rows'];
	$columns = $_POST['columns'];


	echo "<table width='100%' border='1'>";
	for ($i=0; $i < $rows ; $i++) { 
		echo "<tr>";
		for ($j=0; $j < $columns; $j++) { 
			echo "<td> $sno </td>";
			$sno++;
		}
		echo "</tr>";
	}
	echo "</table>";
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Print Table With PHP CODE</title>
</head>
<body>
	<br><br><br><br><br><br><br><br>
	<form action="" method="post">
		<label for="">ROWS</label>
		<input type="number" name="rows" required="required">
		<br><br>
		<label for="">Columns</label>
		<input type="number" name="columns" required="required">
		<br><br>
		<input type="submit" name="print" value="Print Table">
	</form>
</body>
</html>