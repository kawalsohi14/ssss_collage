<?php 
// numeric Array
$array = [10,20,30,40,50,60,70,80,90,100];

echo "<pre>";
//print_r($array);

// assoc

$arrayAsso = [
	'a'	=>	10,
	'b'	=>	20,
	'c'	=>	30,
	'd'	=>	40,
	'e'	=>	50,
	'f'	=>	60
];

echo "<pre>";
//print_r($arrayAsso);


// multiDimen

$students = [
	['name'	=>	'Prabh','rollno' => 10],
	['name'	=>	'Ranjit','rollno' => 10],
	['name'	=>	'Kiran','rollno' => 10],
	['name'	=>	'A','rollno' => 2],
	['name'	=>	'B','rollno' => 3],
	['name'	=>	'C','rollno' => 4],
	['name'	=>	'D','rollno' => 5],
];
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Print Student Detail</title>
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
</head>
<body>
	<table class="table table-hover">
		<tr>
			<th>Name</th>
			<th>Roll No</th>
			<th>Action</th>
		</tr>
		<?php 
		foreach ($students as $key => $value) {
		?>
		<tr>
			<td><?=$value['name']?></td>
			<td><?=$value['rollno']?></td>
			<td>
				<button type="button" class="btn btn-info">Edit</button>
				<button type="button" class="btn btn-info">Delete</button>
			</td>
		</tr>

		<?php	
		}
		 ?>
	</table>
</body>
</html>