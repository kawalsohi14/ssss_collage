<?php
if (isset($_POST['submit'])) {
	$tables = $_POST['tables'];
	if (!empty($tables)) {
		$tableArray = explode(',', $tables);
		foreach ($tableArray as $key => $value) {
			printTable($value);
			echo "<hr>";
		}
	}else{
		echo "Please enter table values";
	}
}

function printTable($table){
	for ($i=1; $i <= 10 ; $i++) { 
		echo $table .'x'. $i .' = '.$table*$i;
		echo "<br>";
	}
}

?>
<form action="" method="post">
	<input type="text" name="tables">
	<input type="submit" name="submit" value="Print">
</form>