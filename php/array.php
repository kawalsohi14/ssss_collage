<?php 
// array
// numeric Array

echo "<pre>";

$array = array('ranjit', 'komal', 'harman');
$array = ['ranjit', 'komal', 'harman'];

print_r($array[2]);
// Asso

echo "<hr>";
$array_asso = ['name' => 'ram', 'class' => '10th', 'address' => 'amritsar'];

print_r($array_asso['name']);


// multi Array
echo "<hr>";
$multiArray = [
	'10' => [
		'name' => 'ram',
		'rollno'	=>	10
	],
	'12'	=>	[
		'name'	=>	'sham',
		'rollno'	=>	20
	]
];

print_r($multiArray['10']['name']);


echo "<hr>";
// data

$data = array();


for ($i=0; $i < 10; $i++) { 
	$data[] = $i;
}
print_r($data);



?>